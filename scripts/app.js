var purit = angular.module('purit', []);

// purit.run(['$rootScope', function ($rootScope) {
// 	// $rootScope.
// 	console.dir('asd');
// }]);

purit.controller('puritController', ['$scope', function($scope){
	//first step is dynamically display the question starting from the first one
	$scope.init = function () {
		console.dir('inited');
	}
	// $scope.currentQuestion = 1;
	// getQuestionTemplate($scope.currentQuestion);
	
	$scope.questions = [
		{
			question: 1,
			template: "questions/question1.html"
		},
		{
			question: 2,
			template: "questions/question2.html"
		},   
		{
			question: 3,
			template: "questions/question3.html"
		},
		{
			question: 50,
			template: "questions/question4.html"
		},             
	];

	$scope.getQuestionTemplate = function(currentQuestion){
		console.dir(currentQuestion);
		return $scope.questions[currentQuestion].template;
		// for (var i = 0; i < $scope.questions.length; i++) {
		// 	if ($scope.currentQuestion == $scope.questions[i].question) {
		// 	}
		// }
	}

	$scope.gotoStep = function(newQuestion) {
		$scope.currentQuestion = newQuestion;
	}
}])