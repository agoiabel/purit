var purit = angular.module('purit', []);

purit.controller('puritController', ['$scope', '$window', function($scope, $window){
	
	$scope.currentQuestion = 1;
		
	/**
	 * you will have to change this answer_option to chose the right answer for each questions
	 * 
	 * @type 
	 */
	$scope.questions = [
		{
			question: 1,
			template: "questions/1.html",
			answer_option: 1
		},
		{
			question: 2,
			template: "questions/2.html",
			answer_option: 1
		},   
		{
			question: 3,
			template: "questions/3.html",
			answer_option: 1
		},
		{
			question: 4,
			template: "questions/4.html",
			answer_option: 1
		},
		{
			question: 5,
			template: "questions/5.html",
			answer_option: 2
		},
		{
			question: 6,
			template: "questions/6.html",
			answer_option: 4
		},
		{
			question: 7,
			template: "questions/7.html",
			answer_option: 1
		},
		{
			question: 8,
			template: "questions/8.html",
			answer_option: 1
		},
		{
			question: 9,
			template: "questions/9.html",
			answer_option: 2
		},
		{
			question: 10,
			template: "questions/10.html",
			answer_option: 3
		},

		{
			question: 11,
			template: "questions/11.html",
			answer_option: 4
		},
		{
			question: 12,
			template: "questions/12.html",
			answer_option: 4
		},
		{
			question: 13,
			template: "questions/13.html",
			answer_option: 2
		},
		{
			question: 14,
			template: "questions/14.html",
			answer_option: 2
		},
		{
			question: 15,
			template: "questions/15.html",
			answer_option: 1
		},
		{
			question: 16,
			template: "questions/16.html",
			answer_option: 2
		},
		{
			question: 17,
			template: "questions/17.html",
			answer_option: 1
		},

		{
			question: 18,
			template: "questions/18.html",
			answer_option: 2
		},
		{
			question: 19,
			template: "questions/19.html",
			answer_option: 2
		},
		{
			question: 20,
			template: "questions/20.html",
			answer_option: 1
		},
		{
			question: 21,
			template: "questions/21.html",
			answer_option: 2
		},
		{
			question: 22,
			template: "questions/22.html",
			answer_option: 1
		},
		{
			question: 23,
			template: "questions/23.html",
			answer_option: 1
		},
		{
			question: 24,
			template: "questions/24.html",
			answer_option: 1
		},

		{
			question: 25,
			template: "questions/25.html",
			answer_option: 2
		},
		{
			question: 26,
			template: "questions/26.html",
			answer_option: 1
		},
		{
			question: 27,
			template: "questions/27.html",
			answer_option: 2
		},
		{
			question: 28,
			template: "questions/28.html",
			answer_option: 2
		},
		{
			question: 29,
			template: "questions/29.html",
			answer_option: 4
		},
		{
			question: 30,
			template: "questions/30.html",
			answer_option: 1
		},
		{
			question: 50,
			template: "questions/50.html",
			answer_option: 1
		}            
	];

	/**
	 * check if this is the landing page
	 * 
	 * @return 
	 */
	$scope.landingPage = function () {
		if ($scope.currentQuestion == 1) {
			return true;
		}
		return false;
	}

	/**
	 * check if first question
	 * 
	 * @return 
	 */
	$scope.firstQuestion = function () {

		for (var i = 0; i < $scope.question_array.length; i++) {
			if ($scope.question_array[i] == $scope.currentQuestion) {
				if ($scope.currentQuestion == $scope.question_array[0]) {
					return true;
				}
				return false;
			}
		}
	}

	/**
	 * check if last question
	 * 
	 * @return 
	 */
	$scope.lastQuestion = function () {
		for (var i = 0; i < $scope.question_array.length; i++) {
			if ($scope.question_array[i] == $scope.currentQuestion) {
				if ($scope.currentQuestion == $scope.question_array[4]) {
					return true;
				}
				return false;
			}
		}	
	}

	/**
	 * check if its the result page
	 * 
	 * @return 
	 */
	$scope.resultPage = function () {
		if ($scope.currentQuestion == 50) {
			return true;
		}
		return false;
	}

	/**
	 * Generate random number
	 * 
	 * @param  min 
	 * @param  max
	 *  
	 * @return 
	 */
	var randomIntFromInterval = function (min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}

	/**
	 * check if value is in array
	 * 
	 * @param  value
	 * @param  array
	 * @return      
	 */
	var isInArray = function (value, array) {
	  return array.indexOf(value) > -1;
	}

	/**
	 * This is use to get the initial template
	 * 
	 * @return 
	 */
	$scope.getQuestionTemplate = function(){
		for (var i = 0; i < $scope.questions.length; i++) {
			if ($scope.currentQuestion == $scope.questions[i].question) {
				return $scope.questions[i].template;
			}
		}
	}

	/**
	 * Initialize question form
	 * 
	 * @type 
	 */
	$scope.questionForm = {};

	/**
	 * set the quiz question to be display to this user and store in an array
	 * 
	 * @return 
	 */
	$scope.startQuestion = function () {
		$scope.question_array = [];

		for (var i = 0; i <= 5; i++) {
			while ($scope.question_array.length < 5) {
				var question_number = randomIntFromInterval(2, 30);
				if (! isInArray(question_number, $scope.question_array)) {
					$scope.question_array.push(question_number);
				}
			}
		}

		$scope.question_array.pop();
		$scope.question_array.push(4)
		
		console.dir($scope.question_array);

		$scope.gotoNextQuestion($scope.question_array[0]);
	}

	/**
	 * Handle the process of going to next question
	 * 
	 * @param newQuestion 
	 * @return
	 */
	$scope.gotoNextQuestion = function(newQuestion) {

		if (typeof newQuestion === "undefined") {

			var currentQuestion = $scope.currentQuestion;

			//get the current question
			for (var i = 0; i < $scope.question_array.length; i++) {
				if ($scope.question_array[i] == currentQuestion) {
					var currentQuestion = $scope.question_array[i];
					var currentQuestionKey = i;
				}
			}

			nextQuestionKey = currentQuestionKey + 1;
			return $scope.gotoNextQuestion($scope.question_array[nextQuestionKey]);
		}

		return $scope.currentQuestion = newQuestion;
	}

	/**
	 * Handle the process of going to next question
	 * 
	 * @param newQuestion 
	 * @return
	 */
	$scope.gotoPreviousQuestion = function(newQuestion) {

		if (typeof newQuestion === "undefined") {

			var currentQuestion = $scope.currentQuestion;
				
			//get the current question
			for (var i = 0; i < $scope.question_array.length; i++) {
				if ($scope.question_array[i] == currentQuestion) {
					var currentQuestion = $scope.question_array[i];
					var currentQuestionKey = i;
				}
			}

			previousQuestionKey = currentQuestionKey - 1;
			
			return $scope.gotoPreviousQuestion($scope.question_array[previousQuestionKey]);
		}

		return $scope.currentQuestion = newQuestion;
	}

	/**
	 * submit the question
	 * 
	 * @return 
	 */
	$scope.submit = function () {
		//i need to calculate the total
		$scope.result = 0;

		for (var key in $scope.questionForm) {
		  if ($scope.questionForm.hasOwnProperty(key)) {

		    var questionNumber = key.split("_")[1];
		    var userAnswer = $scope.questionForm[key];

		    for (var j = 0; j < $scope.questions.length; j++) {
		    	if ($scope.questions[j].question == questionNumber) {
		    		if ($scope.questions[j].answer_option == userAnswer) {
		    			$scope.result = $scope.result + 1;
		    		}
		    	}
		    }

		  }
		}

		/**
		 * Look up for caption base on score, please you will also need to change the caption to your desire words
		 * 
		 * @type 
		 */
		var caption = {
			'0' : 'I got 0% in the Purit Antiseptic ‘How Clean are you quiz’. Find out yours by taking this quiz too! // You are 0% clean!  Take charge and be sure you use Purit Antiseptic daily to help improve your health.',
			'20' : 'I got 20% in the Purit Antiseptic ‘How Clean are you quiz’. Find out yours by taking this quiz too! // You are 0% clean!  Take charge and be sure you use Purit Antiseptic daily to help improve your health.',
			'40' : 'I got 40% in the Purit Antiseptic ‘How Clean are you quiz’. Find out yours by taking this quiz too! // You are 0% clean!  Take charge and be sure you use Purit Antiseptic daily to help improve your health.',
			'60' : 'I got 60% in the Purit Antiseptic ‘How Clean are you quiz’. Find out yours by taking this quiz too! // You are 0% clean!  Take charge and be sure you use Purit Antiseptic daily to help improve your health..',
			'80' : 'I got 80% in the Purit Antiseptic ‘How Clean are you quiz’. Find out yours by taking this quiz too! // You are 0% clean!  Take charge and be sure you use Purit Antiseptic daily to help improve your health.',
			'100' : 'I got 100% in the Purit Antiseptic ‘How Clean are you quiz’. Find out yours by taking this quiz too! // You are 0% clean!  Take charge and be sure you use Purit Antiseptic daily to help improve your health.'
		}

		$scope.result = $scope.result * 20;
		$scope.caption = caption[$scope.result];
	

		return $scope.currentQuestion = 50;
	}

	/**
	 * Post on twitter
	 * 
	 * @return 
	 */
	$scope.postToTwitter = function () {

		var message = $scope.caption.split("!")[0];		

	    var width  = 575,
	        height = 400,
	        left   = ($window.innerWidth  - width)  / 2,
	        top    = ($window.innerHeight - height) / 2,
	        url    = "http://twitter.com/share?text="+encodeURI(message),
	        opts   = 'status=1' +
	                 ',width='  + width  +
	                 ',height=' + height +
	                 ',top='    + top    +
	                 ',left='   + left;
	    
	    $window.open(url, 'twitter', opts);
	}


	/**
	 * Post on facebook
	 * 
	 * @return 
	 */
	$scope.postToFacebook = function () {
		console.dir('post to facebook');

	    FB.init({
	        appId  : '1512503635715078',
	        status : true, // check login status
	        cookie : true, // enable cookies to allow the server to access the session
	        xfbml  : true , // parse XFBML
	        oauth : true // Enable oauth authentication
	    });

	    FB.login(function(response)
	    {
	        if (response.authResponse)
	        {
	            console.dir('Logged in!');
	 
	            // Post message to your wall
	 
	            var opts = {
	                message : $scope.caption,
	                name : 'Post Title',
	                link : 'www.purit.dev',
	                description : 'post description',
	            };
	 
	            FB.api('/me/feed', 'post', opts, function(response)
	            {
	                if (!response || response.error)
	                {
	                    console.dir('Posting error occured');
	                }
	                else
	                {
	                    console.dir('Success - Post ID: ' + response.id);
						swal("Congrat!", "post now on your facebook wall!", "success");
	                }
	            });
	        }
	        else
	        {
	            alert('Not logged in');
	        }
	    }, { scope : 'publish_actions' });
	}


}]);